import React,{useState, useEffect} from 'react';
import { TouchableOpacity, StyleSheet, Animated} from 'react-native';
import colors from '../../config/colors';
import { Avatar, Button, Card, Title, Paragraph, Colors } from 'react-native-paper';
// import { Container } from './styles';

export default function Item({Nome, Preco, Codigo, onLongPress,activeCode, ...rest}) {
  const AnimatedTouchable = Animated.createAnimatedComponent(TouchableOpacity);
  const [color] = useState(new Animated.Value(1));
  useEffect(()=>{
    if(activeCode !== null && activeCode !== Codigo) {
      Animated.timing(color,{
        toValue: 0,
      }).start();
    }
    if(activeCode === null) {
      Animated.timing(color,{
        toValue: 1
      }).start();
    }
  },[activeCode]);
  return (
    <AnimatedTouchable  onLongPress={()=>onLongPress(Codigo)} {...rest} style={[styles.item,
      {
        elevation: color.interpolate({
          inputRange:[0,1],
          outputRange:[0, 20]
        })
        }
    ]}>
    <Card>
    <Card.Title title={Nome} 
     left={(props) => <Avatar.Icon 
     style={{backgroundColor:colors.primary}} color="#fff" {...props} 
     icon="shopping-cart" />} />
    <Card.Content>
     
      <Paragraph>Preço: {Preco}</Paragraph>
    
      <Paragraph>Codigo: {Codigo}</Paragraph>
    </Card.Content>

  </Card>
    </AnimatedTouchable>
  );
}
const styles = StyleSheet.create({

    item:{
       
        borderRadius: 10,
        //position:'absolute',
        zIndex:4,
       
        
        marginVertical: 8,
        marginHorizontal: 16,
    },
    nome: {
        fontSize: 22,
        marginLeft: 30
    },
    preco: {
        fontSize: 18,
        marginLeft: 30
    }

    
  });