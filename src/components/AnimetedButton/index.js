import React, {useState, useEffect} from 'react';
import { TouchableOpacity, View, Animated, StyleSheet } from 'react-native';
import colors from '../../config/colors';
// import { Container } from './styles';
const size = 70;

export default function AnimetedButton({r,alfa,children,show, center,onPress}) {
    const [offsetX] = useState(new Animated.Value(0));
    const [offsetY] = useState(new Animated.Value(0));
    const AnimatedTouchable = Animated.createAnimatedComponent(TouchableOpacity);
    const radianToDeg = deg => {
        return (deg * Math.PI) / 180;
      };
    useEffect(()=>{
      if(!center){
        if(show){
          Animated.timing(offsetX,{
            toValue: (r-size)/2 * Math.cos(radianToDeg(alfa)),
            useNativeDriver:true,
        }).start();
        Animated.timing(offsetY,{
            toValue: (r-size)/2 * Math.sin(radianToDeg(alfa)),
            useNativeDriver:true,
        }).start();
        }else{
          Animated.timing(offsetX,{
            toValue: 0,
            useNativeDriver:true,
        }).start();
        Animated.timing(offsetY,{
            toValue: 0,
            useNativeDriver:true,
        }).start();
        }
      }


    },[show]);
  return (

 <AnimatedTouchable
          onPress={onPress}
          style={[
            styles.button,
            {
              top: r -r/2 -(size/2),
              left: r -r/2 -(size/2) ,//+ (center? 37.5: 0),
              transform: [
                {translateX: offsetX},
                {translateY: offsetY},],
            },
          ]}
          >
      
            {children}
         
     
        </AnimatedTouchable>
   
   
  );
}
const styles = StyleSheet.create({
    button: {
      width: size,
      height: size,
  
      position: 'absolute',

      zIndex:10
      
    },
    TextButton: {
      fontSize: 22,
      color: '#fff',
      padding: 15,
      textAlign: 'center',
      transform: [{translateY: -6}],
    },

  });
  