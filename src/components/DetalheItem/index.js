import React from 'react';
import { View } from 'react-native';
import { Avatar, Button, Card, Title, Paragraph, Colors } from 'react-native-paper';
// import { Container } from './styles';
import colors from '../../config/colors';
export default function DetalheItem({produto, onDelete,onEdit,onFinish}) {
    //<Card.Cover source={{ uri: 'https://picsum.photos/700' }} />
  return (
    <Card>
    <Card.Title title="Detalhe do produto" 
     left={(props) => <Avatar.Icon 
     style={{backgroundColor:colors.primary}} color="#fff" {...props} 
     icon="shopping-cart" />} />
    <Card.Content>
      <Title>Nome: {produto.Nome}</Title>
      <Paragraph>Estoque: {produto.Estoque}</Paragraph>
      <Paragraph>Preço: {produto.Preco}</Paragraph>
      <Paragraph>Validade: {produto.Validade}</Paragraph>
      <Paragraph>Codigo: {produto.Codigo}</Paragraph>
    </Card.Content>
    
    <Card.Actions>
      <Button color={colors.primaryTint} onPress={onDelete}>Excluir</Button>
      <Button color={colors.primaryTint} onPress={onEdit}>Editar</Button>
      <Button color={colors.primaryTint} onPress={onFinish}>Ok</Button>
    </Card.Actions>
  </Card>
  );
}
