import React, { useEffect, useState } from 'react';
import {
    FlatList,
    SafeAreaView,
    Alert,
    Modal,
    TouchableOpacity,
    StyleSheet,
    View,
    Animated,
    TextInput,
    Dimensions,

} from 'react-native'
import Icon from 'react-native-vector-icons/AntDesign';
import colors from '../../config/colors';
import { Button, Paragraph, Menu, Divider, Provider } from 'react-native-paper';
// import { Container } from './styles';
const size = 60;

export default function InputPesquisa({onOpen,onClose,onSearch}) {
    const { width, height } = Dimensions.get('window');
    const [sizePesquisa] = useState(new Animated.Value(0))
    const [icon, setIcon] =useState('search1');
    const [state, setState]  = useState({
        visible: false,
      });
    const [buscarPorNome,setBuscarPorNome] = useState(true)
    const [value,setValue] = useState('')
    const _openMenu = () => setState({ visible: true });
    
    const _closeMenu = () => setState({ visible: false })
    const press = () => {
        
        if(sizePesquisa._value === 0){
            Animated.timing(sizePesquisa,{
                toValue:1
            }).start()
            setIcon('close');
            onOpen();
        } else {
            Animated.timing(sizePesquisa,{
                toValue:0
            }).start()
            setIcon('search1');
            onClose();
        }
    }
    const onChangeText = (text) => {
        if(text === ''){
            onSearch({})
        }
        else if(buscarPorNome){
            onSearch({Nome:text})
        } else {
            onSearch({Codigo: text})
        }
        setValue(text)
    }
    return (
   
        <Animated.View style={{
            flexDirection: 'row',
            width: sizePesquisa.interpolate({
                inputRange: [0, 1],
                outputRange: [size, width - 32]
            }),
            alignItems: 'center',
            
            backgroundColor: colors.warningTint,
            borderRadius: size/2,
            position: 'absolute',
            zIndex: 2,
            position:'absolute',
            top:3,
            right:16,
            overflow:'hidden'


        }}>

            <TouchableOpacity
                style={styles.botaoPesquisa}
                onPress={press}
            >
                <Icon 
                style={styles.TextButton} 
                name={icon} 
                size={25} color="#fff" />
            </TouchableOpacity>

            <TextInput style={{
               
                  width:width-2*size-32,
                  position: 'relative',
                  color:'#fff',
                  fontSize:20
            }} 
                placeholder={buscarPorNome? "Digite o nome":"Digite o codigo"}
                value={value}
                onChangeText={onChangeText} 
            />
        
           
          <Menu
        visible={state.visible}
        onDismiss={_closeMenu}
        style={{ zIndex: 10, position: 'absolute' }}
        anchor={
            <TouchableOpacity
    style={[styles.botaoPesquisa,{zIndex:2, backgroundColor:'#0000'}]}
    onPress={_openMenu}
>
    <Icon 
    style={styles.TextButton} 
    name="down"
   
    size={25} color="#fff" />
</TouchableOpacity>
        }
    >
        <Menu.Item onPress={() => {
            setBuscarPorNome(true)
            _closeMenu()
            }} title="Buscar por nome" />
        <Menu.Item onPress={() => {
            setBuscarPorNome(false)
            _closeMenu()
            }} title="Buscar por codigo" />

    </Menu>
          

        </Animated.View>
       
  

    );
}
const styles = StyleSheet.create({

    botaoPesquisa: {

        width: size,
        height: size,
       
        backgroundColor: colors.warning,
        borderRadius: size/2,
        justifyContent: 'center',
        alignItems: 'center',
        zIndex: 3,
        

    }
})
