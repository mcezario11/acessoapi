import React, {useState, useEffect} from 'react';
import {
  View,

  
  StyleSheet,
  Animated,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import AnimetedButton from '../AnimetedButton';
import Icon from 'react-native-vector-icons/AntDesign';
// import { Container } from './styles';
const { width, height } = Dimensions.get('window');
const d = 300;
import colors from '../../config/colors';
export default function Menu({show,onClose,onDelete,onEdit, onDetalhe}) {
  const [size] = useState(new Animated.Value(0));
  const open = Animated.timing(size,{toValue: d});
  const close =  Animated.timing(size,{toValue: 0})
  const TouchableAnimeated = Animated.createAnimatedComponent(TouchableOpacity);
  
  const alfa = 90;
 useEffect(()=>{
   if(show){
    open.start();
   }else{
   close.start();
   }

 },[show])
  return (
    <View>

   
    <Animated.View
      style={[styles.Menu,{
        right:  -d/2 + 50,
        top:  -d/2  + 50,
        zIndex:4,
        width: size,
        height: size,
        
      }]}>
     
        <AnimetedButton show={show} r={d} alfa={alfa} onPress={onEdit}>
        <Icon  style={styles.TextButton} name="edit" size={32} color="#000" />
        </AnimetedButton>
        <AnimetedButton onPress={onDelete} show={show} r={d} alfa={alfa+45}>
        <Icon style={styles.TextButton} name="delete" size={32} color="#000" />
        </AnimetedButton>
        <AnimetedButton onPress={onDetalhe}  show={show} r={d} alfa={alfa+90}>
        <Icon style={styles.TextButton} name="eye" size={32} color="#000" />
        </AnimetedButton>
        <AnimetedButton show={show} r={d} alfa={alfa+90} center  onPress={onClose} >
        <Icon style={styles.TextButton} name="close" size={32} color="#000" />
        </AnimetedButton>
     

      
    </Animated.View>

    <TouchableAnimeated
      style={{
        right:  -width/2 ,
        top: -height/2 ,
        zIndex:3,
        position:'absolute',
        borderRadius:width/2,
        backgroundColor:'#0006',
        width: size.interpolate({
          inputRange:[0,d],
          outputRange:[0,width*2]
        }),
        height: size.interpolate({
          inputRange:[0,d],
          outputRange:[0,height*2]
        }),
        
      }}
      onPress={()=>onClose()}
      >

      </TouchableAnimeated>
 

      
    </View>
  );
}
const styles = StyleSheet.create({
  button: {
    width: 75,
    height: 75,

    position: 'absolute',
    top: d -d/2 -37.5,
    left: d -d/2 -37.5,
    zIndex:4
    
  },
  TextButton: {

    padding: 15,
    textAlign: 'center',
 
    transform: [{translateY: 0}],
  },
  Menu:{
    
        flex: 1,
      
        
        backgroundColor:'#fff',

        borderRadius: d/2,
        position: 'absolute',
        
      

      
  }
});
