import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import Home from './pages/Home';
import colors from './config/colors'
import Cadastro from './pages/Cadastro';
import Edicao from './pages/Edicao';

const Routers = createAppContainer(
    createStackNavigator(
        {
            Home:{
                screen:Home,
                navigationOptions:{title:'Home'}
            },
            Cadastro:Cadastro,
            Editar: Edicao

        },
        {
            initialRouteName:'Home',
            defaultNavigationOptions: {
                headerStyle: {
                    backgroundColor: colors.primary
                },
                headerTintColor: '#000'
                
              },
              headerLayoutPreset: 'center',
            
        }
    )
);
export default Routers;