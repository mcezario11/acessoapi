import Axios from 'axios';

const api = Axios.create();

const Api = {
    createProduct: ({Nome, Preco, Validade, Estoque}) => {
        return api.post('http://ronepage.com.br/api/cadastraproduto.php',{Nome, Preco, Validade, Estoque})
    },
    updateProduct: ({Nome, Preco, Validade, Estoque, Codigo}) => {
        return api.post('http://ronepage.com.br/api/alteraproduto.php',
        {Nome, Preco, Validade, Estoque, Codigo,
        '0':Codigo,'1':Nome,'2':Preco,'3':Validade,'4':Estoque})
    },
    listProducts: () => api.post('http://ronepage.com.br/api/listaproduto.php',{}),
    getProduct: (req) => api.post('http://ronepage.com.br/api/listaproduto.php',req),
    deleteProduct: (Codigo) => api.post('http://ronepage.com.br/api/excluiproduto.php',{Codigo})

};

export default Api;