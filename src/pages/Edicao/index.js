import React, {useState,useEffect} from 'react';
import { View,StyleSheet, Button,DatePickerAndroid, TouchableOpacity } from 'react-native';
import { TextInput, Snackbar } from 'react-native-paper';
import colors from '../../config/colors';
import Icon from 'react-native-vector-icons/AntDesign';
import Api from '../../services/api';

// import { Container } from './styles';

export default function Edicao({navigation}) {
  //const lista = ['Nome', 'Preco', 'Estoque']
  //const [editar, setEditar] = useState(false);
  const [snack, setSnack] = useState({ visible: false, title: '' });
  const [editar, setEditar] = useState( {
    Nome:'',
    Estoque:'',
    Preco:'',
    Validade:''
  })
  useEffect(()=>{
      let edit = navigation.getParam('edit');
      setEditar(edit)
  },[])
  datepicker = async () => {
    try {
      const {action, year, month, day} = await DatePickerAndroid.open({
        // Use `new Date()` for current date.
        // May 25 2020. Month 0 is January.
        date: Date.now(),
      });
      if (action !== DatePickerAndroid.dismissedAction) {
        setEditar({...editar, Validade: `${year}-${month}-${day}`})
      }
    } catch ({code, message}) {
      setSnack({ visible: true, title: 'Erro ao pegar a data' });
      console.log(code,message)
      setTimeout(() => {
        setSnack({ visible: false })
      }, 2500);
    }
  }
  const update = async () => {
    let resp = await Api.updateProduct(editar);
    console.log(editar)
 
    
    if (resp.data.resultado === 'ok') {
      navigation.navigate('Home')
    } else {
      setSnack({ visible: true, title: 'Erro ao Editar' });
      setTimeout(() => {
        setSnack({ visible: false })
      }, 2500);
    }
    
  }
  return (
    <View style={styles.form}>
    <Snackbar
        visible={snack.visible}
        onDismiss={() => setSnack({ visible: false })}
        action={{
          label: 'OK',
          onPress: () => {
            setSnack({ visible: false })
          },
        }}
      >
        {snack.title}
      </Snackbar>
     
         <TextInput 
            value={editar.Nome+''}
            onChangeText={(Nome)=>{setEditar({...editar, Nome})}} 
            mode="outlined" style={styles.input} label="Nome"/>
        <TextInput 
            keyboardType="numeric"
            value={editar.Preco+''}
            onChangeText={(Preco)=>{setEditar({...editar, Preco})}} 
            mode="outlined" style={styles.input} label="Preço"/>
        <TextInput
            keyboardType="numeric"
            value={editar.Estoque+''}
            onChangeText={(Estoque)=>{setEditar({...editar, Estoque})}} 
            mode="outlined" style={styles.input} label="Estoque"/>
          
        
        <Button onPress={datepicker} title="Validade" />
        <View style={styles.botaoConteiner}>
        <TouchableOpacity
        style={styles.botao}
        onPress={async ()=>{
          await update()
        }}
      >
        <Icon  style={styles.TextButton} name="check" size={50} color="#fff" />
      </TouchableOpacity>
        </View>
        

    </View>
  );
}
const styles = StyleSheet.create({
    form:{
      margin: 20,
      flex:1
    },
    input:{
      margin:10
    },
    botao: {
     
      width: 80,
      height: 80,
      backgroundColor: colors.primaryShade,
      borderRadius: 50,
      justifyContent:'center',
      alignItems:'center',
      zIndex:3
    },
    botaoConteiner:{
      position: 'absolute',
      bottom: 25,
      right: 25,
    }
  
  });
Edicao.navigationOptions={title:'Edição'}