import React, { useEffect, useState } from 'react';
import {
  FlatList,
  SafeAreaView,
  Alert,
  Modal,
  TouchableOpacity,
  StyleSheet,
  View,
  Animated,
  TextInput,
  Dimensions,

} from 'react-native'
import Item from '../../components/Item'
import Menu from '../../components/Menu'
import Api from '../../services/api';
import { Snackbar, Provider } from 'react-native-paper';
import colors from '../../config/colors';
import Icon from 'react-native-vector-icons/AntDesign';
import { withNavigationFocus } from 'react-navigation';
import DetalheItem from '../../components/DetalheItem';
import InputPesquisa from '../../components/InputPesquisa';
const { width, height } = Dimensions.get('window');
function Home({ navigation, isFocused }) {
  const [lista, setLista] = useState([]);
  const [menu, setMenu] = useState(false);
  const [activeCode, setActiveCode] = useState(null);
  const [snack, setSnack] = useState({ Visible: false, title: '' });
  const [detalhe, setDetalhe] = useState({ show: false, produto: {} })
  const SafeAreaViewAnimated = Animated.createAnimatedComponent(SafeAreaView)
  const [top] = useState(new Animated.Value(0));
  const open = Animated.timing(top,{toValue:60})
  const close = Animated.timing(top,{toValue:0})

  async function updateData() {
    let d = await Api.listProducts();
    // console.log(d)
    setLista(d.data.dados);
  }
  useEffect(() => {

    if (isFocused) {
      updateData();
    }
  }, [isFocused])

  const Close = () => {
    setMenu(false);
    setActiveCode(null);
  }
  const Confirmacao = (Title, msg) => {
    return new Promise((resolve, reject) => {
      Alert.alert(
        Title,
        msg,
        [
          {
            text: 'Cancel',
            onPress: () => resolve(false),
            style: 'cancel',
          },
          {
            text: 'OK', onPress: () => resolve(true)

          },
        ],
        { cancelable: false },
      );
    })
  }
  const Delete = async () => {
    if (activeCode !== null) {
      const c = await Confirmacao('Deseja excluir esse produto?', 'Essa ação não pode ser desfeita');
      Close();
      if (c) {
        let d = await Api.deleteProduct(activeCode);
        //console.log(d)
        if (d.data.resultado === 'ok') {
          await updateData();
          setSnack({ visible: true, title: 'Produto excluido com sucesso!' })
        } else {
          setSnack({ visible: true, title: 'Erro ao excluir o produto!' })
        }
        setTimeout(() => {
          setSnack({ visible: false })
        }, 2500);

      }


    }
  }
  const Edit = () => {
    Close();
    let i = lista.findIndex(p => p.Codigo === activeCode);
    //alert(i)
    if (i !== undefined) {
      navigation.navigate('Editar', { edit: lista[i] })
    }

  }
  const LongPress = (Codigo) => {
    setActiveCode(Codigo)
    setMenu(true)

  }
  const Detalhe = () => {
    let i = lista.findIndex(p => p.Codigo === activeCode);
    setDetalhe({ show: true, produto: lista[i] })
  }
  const Search = async (pesquisa) => {
    let resp = await Api.getProduct(pesquisa);
    
    
      setLista(resp.data.dados);
    
    
  }

  return (
    <Provider>
    <View style={{flex:1}}>
     
          <InputPesquisa 
            onOpen={() => {open.start()}}
            onClose={() => {close.start()}}
            onSearch={Search}
          />
     
    
      <SafeAreaViewAnimated style={{ flex: 1,top:top }}>
        <TouchableOpacity
          style={styles.botao}
          onPress={() => navigation.navigate('Cadastro')}
        >
          <Icon style={styles.TextButton} name="plus" size={50} color="#fff" />
        </TouchableOpacity>
        <Menu show={menu} onClose={Close}
          onDelete={Delete}
          onEdit={Edit}
          onDetalhe={Detalhe}
        />
        <Snackbar
          visible={snack.visible}
          onDismiss={() => setSnack({ visible: false })}
          action={{
            label: 'OK',
            onPress: () => {
              setSnack({ visible: false })
            },
          }}
        >
          {snack.title}
        </Snackbar>


        <Modal
          animationType="slide"
          transparent={false}
          visible={detalhe.show}
          onRequestClose={() => {
            setDetalhe({ show: false })
          }}>
          <DetalheItem produto={detalhe.produto}
            onDelete={() => {
              Delete();
              setDetalhe({ show: false })
            }
            }
            onEdit={() => {
              Edit();
              setDetalhe({ show: false })
            }
            } onFinish={() => { setDetalhe({ show: false }); Close() }} />
        </Modal>

        <FlatList
          data={lista}
          renderItem={({ item }) =>
            <Item
              Nome={item.Nome}
              Preco={item.Preco}
              Codigo={item.Codigo}
              activeCode={activeCode}
              onLongPress={LongPress}
            />}
          keyExtractor={item => item.Codigo}
        />




      </SafeAreaViewAnimated>
    </View>
    </Provider>

  );
}
const styles = StyleSheet.create({
  botao: {
    position: 'absolute',
    bottom: 25,
    right: width/2 -40,
    width: 80,
    height: 80,
    backgroundColor: colors.primaryShade,
    borderRadius: 50,
    justifyContent: 'center',
    alignItems: 'center',
    zIndex: 3
  },
  botaoPesquisa:{
   
    width: 40,
    height: 40,
    backgroundColor: colors.primaryShade,
    borderRadius: 50,
    justifyContent: 'center',
    alignItems: 'center',
    zIndex: 3
  }
})

export default withNavigationFocus(Home);