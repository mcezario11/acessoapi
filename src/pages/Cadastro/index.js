import React, {useState,useEffect} from 'react';
import { 
  View,
  StyleSheet,
  Button,
  DatePickerAndroid,
  TouchableOpacity,
  Keyboard } from 'react-native';
import { TextInput, Snackbar } from 'react-native-paper';
import colors from '../../config/colors';
import Icon from 'react-native-vector-icons/AntDesign';
import Api from '../../services/api';
// import { withNavigationFocus } from 'react-navigation';

// import { Container } from './styles';

export default function Cadastro({navigation}) {
  //const lista = ['Nome', 'Preco', 'Estoque']
 
  const [snack, setSnack] = useState({ visible: false, title: '' });
  const [cadastro, setCadastro] = useState( {
    Nome:'',
    Estoque:'',
    Preco:'',
    Validade:''
  });
  const [show, setShow] = useState(true);
  const inputs = {preco:null,estoque:null}

  datepicker = async () => {
    try {
      const {action, year, month, day} = await DatePickerAndroid.open({
        // Use `new Date()` for current date.
        // May 25 2020. Month 0 is January.
        date: Date.now(),
      });
      if (action !== DatePickerAndroid.dismissedAction) {
        setCadastro({...cadastro, Validade: `${year}-${month}-${day}`})
      }
    } catch ({code, message}) {
      setSnack({ visible: true, title: 'Erro ao pegar a data' });
      setTimeout(() => {
        setSnack({ visible: false })
      }, 2500);
    }
  }
  cadastrar = async () => {

       
      resp = await Api.createProduct(cadastro);
      
    
    
    if (resp.data.resultado === 'ok') {
      navigation.navigate('Home')
    } else {
      setSnack({ visible: true, title: 'Erro ao cadastrar' });
      setTimeout(() => {
        setSnack({ visible: false })
      }, 2500);
    }
    
  }
  return (
    <View style={styles.form}>
    <Snackbar
        visible={snack.visible}
        onDismiss={() => setSnack({ visible: false })}
        action={{
          label: 'OK',
          onPress: () => {
            setSnack({ visible: false })
          },
        }}
      >
        {snack.title}
      </Snackbar>
      <View style={styles.inputs}>
          <TextInput 
              value={cadastro.Nome+''}
              onChangeText={(Nome)=>{setCadastro({...cadastro, Nome})}} 
              mode="outlined" style={styles.input} label="Nome"
              onSubmitEditing={()=>inputs.preco.focus()}
              />
          <TextInput 
              ref={(i)=> inputs.preco = i}
              onSubmitEditing={()=>inputs.estoque.focus()}
              keyboardType="numeric"
              value={cadastro.Preco+''}
              onChangeText={(Preco)=>{setCadastro({...cadastro, Preco})}} 
              mode="outlined" style={styles.input} label="Preço"/>
          <TextInput
              ref={(i)=> inputs.estoque = i}
              keyboardType="numeric"
              value={cadastro.Estoque+''}
              onChangeText={(Estoque)=>{setCadastro({...cadastro, Estoque})}} 
              mode="outlined" style={styles.input} label="Estoque"/>
            
        </View>
        <Button onPress={datepicker} title="Validade" />
        <View style={styles.botaoConteiner}>
        <TouchableOpacity
        style={styles.botao}
        onPress={async ()=>{
          await cadastrar()
        }}
      >
        <Icon  name="check" size={50} color="#fff" />
      </TouchableOpacity>
        </View>
        

    </View>
  );
}
const styles = StyleSheet.create({
  form:{
    margin: 20,
    flex:1,
    flexDirection:'column',
    justifyContent:'space-around'
  },
  input:{
    margin:10
  },
  botao: {
   
    width: 80,
    height: 80,
    backgroundColor: colors.primaryShade,
    borderRadius: 50,
    justifyContent:'center',
    alignItems:'center',
    zIndex:3
  },
  inputs:{
    
  },
  botaoConteiner:{
    alignItems:'flex-end'
  }

});
Cadastro.navigationOptions={title:'Cadastro'}
// withNavigationFocus(Cadastro)