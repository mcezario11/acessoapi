const colors = {
    primary: "#2ECC71",
   
    primaryContrasr: "#000000",
    
    primaryShade: "#28b463",
    primaryTint: "#43d17f",
  
    secondary: "#3498DB",
    
    secondaryContrasr: "#000000",
   
    secondaryShade: "#2e86c1",
    secondaryTint: "#48a2df",
  
    tertiary: "#F1C40F",
    
    tertiaryContrasr: "#000000",
    
    tertiaryShade: "#d4ac0d",
    tertiaryTint: "#f2ca27",
  
    success: "#10dc60",
    
    successContrasr: "#ffffff",
    
    successShade: "#0ec254",
    successTint: "#28e070",
  
    warning: "#ffce00",
    
    warningContrasr: "#ffffff",
    
    warningShade: "#e0b500",
    warningTint: "#ffd31a",
  
    danger: "#f04141",
    
    dangerContrasr: "#ffffff",
    
    dangerShade: "#d33939",
    dangerTint: "#f25454",
  
    dark: "#222428",
    
    darkContrasr: "#ffffff",
    
    darkShade: "#1e2023",
    darkTint: "#383a3e",
  
    medium: "#989aa2",
    
    mediumContrasr: "#ffffff",
    
    mediumShade: "#86888f",
    mediumTint: "#a2a4ab",
  
    light: "#f4f5f8",
    
    lightContrasr: "#000000",
    
    lightShade: "#d7d8da",
    lightTint: "#f5f6f9",
  }
  export default colors;